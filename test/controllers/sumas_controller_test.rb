require 'test_helper'

class SumasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @suma = sumas(:one)
  end

  test "should get index" do
    get sumas_url
    assert_response :success
  end

  test "should get new" do
    get new_suma_url
    assert_response :success
  end

  test "should create suma" do
    assert_difference('Suma.count') do
      post sumas_url, params: { suma: { dos: @suma.dos, res: @suma.res, uno: @suma.uno } }
    end

    assert_redirected_to suma_url(Suma.last)
  end

  test "should show suma" do
    get suma_url(@suma)
    assert_response :success
  end

  test "should get edit" do
    get edit_suma_url(@suma)
    assert_response :success
  end

  test "should update suma" do
    patch suma_url(@suma), params: { suma: { dos: @suma.dos, res: @suma.res, uno: @suma.uno } }
    assert_redirected_to suma_url(@suma)
  end

  test "should destroy suma" do
    assert_difference('Suma.count', -1) do
      delete suma_url(@suma)
    end

    assert_redirected_to sumas_url
  end
end
