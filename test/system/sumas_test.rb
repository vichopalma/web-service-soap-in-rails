require "application_system_test_case"

class SumasTest < ApplicationSystemTestCase
  setup do
    @suma = sumas(:one)
  end

  test "visiting the index" do
    visit sumas_url
    assert_selector "h1", text: "Sumas"
  end

  test "creating a Suma" do
    visit sumas_url
    click_on "New Suma"

    fill_in "Dos", with: @suma.dos
    fill_in "Res", with: @suma.res
    fill_in "Uno", with: @suma.uno
    click_on "Create Suma"

    assert_text "Suma was successfully created"
    click_on "Back"
  end

  test "updating a Suma" do
    visit sumas_url
    click_on "Edit", match: :first

    fill_in "Dos", with: @suma.dos
    fill_in "Res", with: @suma.res
    fill_in "Uno", with: @suma.uno
    click_on "Update Suma"

    assert_text "Suma was successfully updated"
    click_on "Back"
  end

  test "destroying a Suma" do
    visit sumas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Suma was successfully destroyed"
  end
end
