class CreateSumas < ActiveRecord::Migration[5.2]
  def change
    create_table :sumas do |t|
      t.integer :uno
      t.integer :dos
      t.integer :res

      t.timestamps
    end
  end
end
