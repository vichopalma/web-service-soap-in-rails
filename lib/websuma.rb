class Websuma
  attr_reader :add_result

  def initialize(uno, dos)
    client = Savon.client(wsdl: "http://www.dneonline.com/calculator.asmx?WSDL")

    message = { intA: uno, intB: dos  }
    response = client.call(:add, message: message)

    if response.success?
      data = response.to_array(:add_response).first
      if data
        @add_result = data[:add_result]
      end
    end
  end
end
