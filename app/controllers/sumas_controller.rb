class SumasController < ApplicationController
  before_action :set_suma, only: [:show, :edit, :update, :destroy]

  # GET /sumas
  # GET /sumas.json
  def index
    @sumas = Suma.all
  end

  # GET /sumas/1
  # GET /sumas/1.json
  def show

    @suma = Suma.last
    @respuestasuma = Websuma.new(@suma.uno.to_s, @suma.dos.to_s)
    @suma.add_result = @respuestasuma.add_result
    @suma.save


  end

  # GET /sumas/new
  def new
    @suma = Suma.new
  end

  # GET /sumas/1/edit
  def edit
  end

  # POST /sumas
  # POST /sumas.json
  def create
    @suma = Suma.new(suma_params)

    respond_to do |format|
      if @suma.save
        format.html { redirect_to @suma, notice: 'Suma was successfully created.' }
        format.json { render :show, status: :created, location: @suma }
      else
        format.html { render :new }
        format.json { render json: @suma.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sumas/1
  # PATCH/PUT /sumas/1.json
  def update
    respond_to do |format|
      if @suma.update(suma_params)
        format.html { redirect_to @suma, notice: 'Suma was successfully updated.' }
        format.json { render :show, status: :ok, location: @suma }
      else
        format.html { render :edit }
        format.json { render json: @suma.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sumas/1
  # DELETE /sumas/1.json
  def destroy
    @suma.destroy
    respond_to do |format|
      format.html { redirect_to sumas_url, notice: 'Suma was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_suma
      @suma = Suma.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def suma_params
      params.require(:suma).permit(:uno, :dos, :add_result)
    end
end
