json.extract! suma, :id, :uno, :dos, :res, :created_at, :updated_at
json.url suma_url(suma, format: :json)
